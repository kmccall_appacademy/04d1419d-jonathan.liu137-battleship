class Board
  attr_reader :grid, :count, :ships

  def self.default_grid
    default = []
    10.times do
      row = []
      10.times { row << nil }
      default << row
    end

    default
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
    @ships = [
      Ship.new("Aircraft carrier", 5, []),
      Ship.new("Battleship", 4, []),
      Ship.new("Submarine", 3, []),
      Ship.new("Cruiser", 3, []),
      Ship.new("Destroyer", 2, [])
    ]
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def display
    puts @grid
  end

  def count
    ship_count = 0
    (0...@grid.length).each do |row|
      (0...@grid[row].length).each do |col|
        ship_count += 1 if self[[row, col]] == :s
      end
    end

    ship_count
  end

  def populate_grid
    place_random_ship until full?
  end

  def place_ship(ship, setup_arr)
    plus = 0
    if setup_arr[0] == 'h'
      ship.length.times do
        @grid[setup_arr[1][0]][setup_arr[1][1] + plus] = :s
        ship.coords << [setup_arr[1][0], setup_arr[1][1] + plus]
        plus += 1
      end
    elsif setup_arr[0] == 'v'
      ship.length.times do
        @grid[setup_arr[1][0] + plus][setup_arr[1][1]] = :s
        ship.coords << [setup_arr[1][0] + plus, setup_arr[1][1]]
        plus += 1
      end
    end
  end

  def empty?(pos = nil)
    if pos.nil?
      count.zero? ? true : false
    else
      self[pos].nil? ? true : false
    end
  end

  def full?
    return true if count == @grid.length * @grid[0].length
    false
  end

  def place_random_ship
    row, col = rand(@grid.length), rand(@grid[0].length)
    raise "Board is full" if full?

    until empty?([row, col])
      row, col = rand(@grid.length), rand(@grid[0].length)
    end
    self[[row, col]] = :s
  end

  def remove_ship(ship)
    @ships.delete(ship)
  end

  def won?
    return true if count.zero?
    false
  end
end

class Ship
  attr_reader :name, :length
  attr_accessor :coords

  def initialize(name, length, location = [])
    @name = name
    @length = length
    @coords = location
  end

  def fits?(board, orientation, position)
    return v_fit?(board, position) if orientation == 'v'
    return h_fit?(board, position) if orientation == 'h'
  end

  def v_fit?(board, position)
    if (position[0]...(position[0] + length)).to_a.any? do |row|
      board.grid[row][position[1]] == :s
    end || position[0] + length > 10
      false
    else
      true
    end
  end

  def h_fit?(board, position)
    if (position[1]...(position[1] + length)).to_a.any? do |col|
      board.grid[position[0]][col] == :s
    end || position[1] + length > 10
      false
    else
      true
    end
  end

  def destroyed?(board)
    @coords.all? { |pos| board[pos] == :hit }
  end
end
