class HumanPlayer
  attr_reader :name, :board

  def initialize(name, board = Board.new)
    @name = name
    @board = board
  end

  def setup(ship)
    setup_arr = [nil, nil]
    setup_arr[0] = get_orientation(ship)
    setup_arr[1] = get_coords(ship, setup_arr[0])
    setup_arr
  end

  def get_orientation(ship)
    puts "How would you like to orient your #{ship.name} (length #{ship.length})?"
    print "Enter 'h' or 'v' for horizontal or vertical: "
    orientation = gets.chomp[0].downcase
    unless ['v', 'h'].include?(orientation)
      puts "That is not a valid orientation."
      get_orientation(ship)
    end

    orientation
  end

  def get_coords(ship, orientation)
    puts "Where would you like to place your #{ship.name}?"
    print "Enter the top/left-most coordinates in the form 0, 0: "
    coords = gets.chomp.split(',').map(&:to_i)[0..1]

    unless (0..9).cover?(coords[0]) && (0..9).cover?(coords[1])
      puts "Those are not valid coordinates."
      get_coords(ship, orientation)
    end

    unless ship.fits?(board, orientation, coords)
      puts "Your ship will not fit at that location!"
      get_coords(ship, orientation)
    end

    coords[0..1]
  end

  def get_play
    print "Please enter coordinates for your attack in the form 0, 0: "
    move = gets.chomp.split(',').map(&:to_i)[0..1]
    until (0..9).cover?(move[0]) && (0..9).cover?(move[1])
      print "That is not a valid move. Please enter valid coordinates: "
      move = gets.chomp.split(',').map(&:to_i)[0..1]
    end

    move
  end
end

class ComputerPlayer
  attr_reader :name, :board, :attacks

  def initialize(name = "AI", board = Board.new)
    @name = name
    @board = board
    @attacks = []
  end

  def setup(ship)
    setup_arr = []
    setup_arr << get_orientation

    if setup_arr[0] == 'v'
      setup_arr[1] = [rand(10 - ship.length), rand(10)]
      until ship.fits?(board, 'v', setup_arr[1])
        setup_arr[1] = [rand(10 - ship.length), rand(10)]
      end
    elsif setup_arr[0] == 'h'
      setup_arr[1] = [rand(10), rand(10 - ship.length)]
      until ship.fits?(board, 'h', setup_arr[1])
        setup_arr[1] = [rand(10 - ship.length), rand(10)]
      end
    end

    setup_arr
  end

  def get_orientation
    orientation = rand(2)
    if orientation == 0
      'h'
    elsif orientation == 1
      'v'
    end
  end

  def get_play
    move = [rand(10), rand(10)]
    until !attacks.include?(move)
      move = [rand(10), rand(10)]
    end
    @attacks << move
    move
  end
end
