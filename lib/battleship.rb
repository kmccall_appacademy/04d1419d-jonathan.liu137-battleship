require_relative 'board'
require_relative 'player'
require 'io/console'

class BattleshipGame
  attr_reader :p1, :p2, :current, :other

  def initialize(player1, player2)
    @p1 = player1
    @p2 = player2
    @current = p1
    @other = p2
    @last_move = ""
  end

  def play
    setup(@p1)
    switch_clear
    setup(@p2)
    switch_clear

    while !game_over?
      play_turn
      switch_player!
      display_last_move
    end

    @current = winner
    @other = loser

    display_opponent
    display_board
    game_end_message
  end

  def setup(player)
    player.board.ships.each do |ship|
      display_board if player.is_a?(HumanPlayer)
      player.board.place_ship(ship, player.setup(ship))
    end
  end

  def switch_clear
    switch_player!
    if @other.is_a?(HumanPlayer)
      system "clear"
      press_any_key
    end
  end

  def play_turn
    display_status
    move = @current.get_play

    until new_move?(move)
      puts "You have already attacked that location!"
      move = @current.get_play
    end

    attack(move)
    sunk_ship
  end

  def new_move?(move)
    !%i{x hit}.include?(@other.board[move])
  end

  def sunk_ship
    @other.board.ships.each do |ship|
      if ship.destroyed?(@other.board)
        @other.board.remove_ship(ship)
        return ship
      end
    end
  end

  def sunk_ship?
    @other.board.ships.any? { |ship| ship.destroyed?(@other.board) }
  end

  def valid_move?(pos)
    return false if !(0...@other.board.grid.length).cover?(pos[0]) ||
      !(0...@other.board.grid[0].length).cover?(pos[1])
    return true if @other.board[pos].nil? || @other.board[pos] == :s
    false
  end

  def attack(pos)
    if @other.board[pos] == :s
      @other.board[pos] = :hit
      @last_move = "#{current.name} has scored a hit at #{pos}!\n"

      if sunk_ship?
        @last_move << "#{current.name} sank #{other.name}'s #{sunk_ship.name}!"
      end

      puts @last_move
      press_any_key if @current.is_a?(HumanPlayer)
    elsif @other.board.empty?(pos)
      @other.board[pos] = :x
      @last_move = "#{current.name} attacked #{pos} and missed!"
      puts @last_move
      press_any_key if @current.is_a?(HumanPlayer)
    end
  end

  def display_status
    if @current.is_a?(HumanPlayer)
      display_opponent
      display_board
      puts "There are #{other.board.count} enemy ship tiles remaining."
    end
  end

  def display_board
    header = "\n#{current.name}'s current board:"
    top_row = "  0 1 2 3 4 5 6 7 8 9"
    rows = [
      "0                    ",
      "1                    ",
      "2                    ",
      "3                    ",
      "4                    ",
      "5                    ",
      "6                    ",
      "7                    ",
      "8                    ",
      "9                    "
    ]

    (0...@current.board.grid.length).each do |row|
      (0...@current.board.grid[0].length).each do |col|
        if @current.board.grid[row][col] == :x
          rows[row][2 * (col + 1)] = 'x'
        elsif @current.board.grid[row][col] == :s
          rows[row][2 * (col + 1)] = 's'
        elsif @current.board.grid[row][col] == :hit
          rows[row][2 * (col + 1)] = '$'
        end
      end
    end

    puts header
    puts top_row
    puts rows
  end

  def display_opponent
    header = "\n#{current.name}'s previous attacks:"
    top_row = "  0 1 2 3 4 5 6 7 8 9"
    rows = [
      "0                    ",
      "1                    ",
      "2                    ",
      "3                    ",
      "4                    ",
      "5                    ",
      "6                    ",
      "7                    ",
      "8                    ",
      "9                    "
    ]

    (0...@other.board.grid.length).each do |row|
      (0...@other.board.grid[0].length).each do |col|
        if @other.board.grid[row][col] == :x
          rows[row][2 * (col + 1)] = 'x'
        elsif @other.board.grid[row][col] == :hit
          rows[row][2 * (col + 1)] = '$'
        end
      end
    end

    puts header
    puts top_row
    puts rows
  end

  def switch_player!
    if current == p1
      @current = p2
      @other = p1
    else
      @current = p1
      @other = p2
    end

  end

  def display_last_move
    if @current.is_a?(HumanPlayer)
      system "clear"
      press_any_key if @other.is_a?(HumanPlayer)
      puts @last_move
    end
  end

  def press_any_key
    print "#{current.name}, press any key to continue"
    STDIN.getch
    system "clear"
  end

  def count
    board.count
  end

  def game_over?
    return true if p1.board.won? || p2.board.won?
    false
  end

  def winner
    return @p1 if p2.board.won?
    return @p2 if p1.board.won?
  end

  def loser
    return @p1 if p1.board.won?
    return @p2 if p2.board.won?
  end

  def game_end_message
    puts "\n#{current.name} has sunk all of #{other.name}'s ships!"
    puts "#{current.name} wins!"
  end

end

def only_yes_no
  response = gets.chomp[0].downcase
  until ['y', 'n'].include?(response)
    puts "That is not a valid response. Please type 'y' or 'n'."
    response = gets.chomp[0].downcase
  end

  response
end

if $PROGRAM_NAME == __FILE__
  print "Will Player 1 be a Human Player (y/n)? "
  p1_human = only_yes_no
  print "What is Player 1's name? "
  p1_name = gets.chomp

  p1 = HumanPlayer.new(p1_name) if p1_human == 'y'
  p1 = ComputerPlayer.new(p1_name) if p1_human == 'n'

  print "Will Player 2 be a Human Player (y/n)? "
  p2_human = only_yes_no
  print "What is Player 2's name? "
  p2_name = gets.chomp

  p2 = HumanPlayer.new(p2_name) if p2_human == 'y'
  p2 = ComputerPlayer.new(p2_name) if p2_human == 'n'

  BattleshipGame.new(p1, p2).play
end
